# server-ports repository

This git repository contains sources for a [COPR repository](https://copr.fedorainfracloud.org/coprs/jstanek/server-ports/)
of server-related software that I use/intend to use,
and that is either not present in CentOS 7 or EPEL,
or has required updates not present in those.
