# Inspired by ralph/taskd
%global pkgname taskd

Name:       khardix-%{pkgname}
Version:    1.1.0
Release:    3%{?dist}
Summary:    Alternative distribution of taskwarrior server

Group:      Application/Productivity
License:    MIT
URL:        https://taskwarrior.org/docs/#taskd

Source0:    https://taskwarrior.org/download/%{pkgname}-%{version}.tar.gz
Source1:    config
Source3:    logrotate.conf
Source4:    taskd.service
Source5:    taskd.xml

Patch0:     enforce-system-crypto-policies.patch
Patch1:     generate-system-paths.patch

BuildRequires: cmake
BuildRequires: firewalld-filesystem
BuildRequires: gcc-c++
BuildRequires: gnutls-devel
BuildRequires: libuuid-devel
BuildRequires: shadow-utils
BuildRequires: systemd

# For certificate generation
Requires: which gnutls-utils

Requires: systemd
%{?systemd_requires}
Requires: firewalld-filesystem
Requires(post): firewalld-filesystem

Conflicts: %{pkgname}
Provides: %{pkgname} = %{version}

# Define common path macros
%global taskd_datadir %(systemd-path system-state-private)/%{pkgname}
%global taskd_confdir %{_sysconfdir}/%{pkgname}
%global taskd_extdir %{_libexecdir}/%{pkgname}
%global taskd_logdir %{_localstatedir}/log/%{pkgname}

%global pki_certdir %{_sysconfdir}/pki/%{pkgname}
%global pki_scriptdir %(systemd-path system-library-private)/%{pkgname}/pki

%global firewalld_servicedir %(systemd-path system-library-private)/firewalld/services

%description
The Taskserver is a lightweight, secure server providing multi-user,
multi-client access to task data. This allows true syncing between desktop and
mobile clients.

Users want task list access from multiple devices running software of differing
sophistication levels to synchronize data seamlessly. Synchronization requires
the ability to exchange transactions between devices that may not have
continuous connectivity, and may not have feature parity.

The Taskserver provides this and builds a framework to go several steps beyond
merely synchronizing data.


%prep
%setup -q -n %{pkgname}-%{version}

# Can be applied only if the OS supports it
%if %{?fedora}%{?!fedora:0} >= 21 || %{?rhel}%{?!rhel:0} > 7
%patch0 -p1 -b.crypto
%endif

%patch1 -p1

%build
%cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DTASKD_DOCDIR="%{_pkgdocdir}" .
cmake --build . -- %{?_smp_mflags}

%install
# Binaries and documentation
DESTDIR="%{buildroot}" cmake --build "$PWD" --target install
rm -f "%{buildroot}%{_pkgdocdir}/INSTALL"

# Main data directory + expected subdirectories
mkdir --parents "%{buildroot}%{taskd_datadir}/orgs"

# Configuration directory
mkdir --parents "%{buildroot}%{taskd_confdir}"
install -Dm 0644 -t "%{buildroot}%{taskd_confdir}" "%{SOURCE1}"
# taskd expects the configuration in it's data dir
ln -sf -t "%{buildroot}%{taskd_datadir}" "%{taskd_confdir}/config"

# Certificate generation and configuration scripts
mkdir --parents "%{buildroot}%{pki_scriptdir}"
install -Dm 0755 -t "%{buildroot}%{pki_scriptdir}" pki/generate*
install -Dm 0644 -t "%{buildroot}%{pki_scriptdir}" pki/README pki/paths
install -Dm 0644 -t "%{buildroot}%{taskd_confdir}" pki/vars
ln -sf           -t "%{buildroot}%{pki_scriptdir}" "%{taskd_confdir}/vars"
# Certificate storage directory
mkdir --parents "%{buildroot}%{pki_certdir}"

# Extensions directory
mkdir --parents "%{buildroot}%{taskd_extdir}"

# Log directories and logrotate config
mkdir --parents "%{buildroot}%{taskd_logdir}"
install -Dm 0644 "%{SOURCE3}" "%{buildroot}%{_sysconfdir}/logrotate.d/%{name}"

# SystemD unit file
mkdir --parents "%{buildroot}%{_unitdir}"
install -Dm 0644 -t "%{buildroot}%{_unitdir}" "%{SOURCE4}"

# FirewallD service file
mkdir --parents "%{buildroot}%{firewalld_servicedir}"
install -Dm 0644 -t "%{buildroot}%{firewalld_servicedir}" "%{SOURCE5}"

%pre
getent passwd %{pkgname} >/dev/null || useradd \
    --comment "Task server system user" \
    --home-dir "%{taskd_datadir}" \
    --shell /sbin/nologin \
    --system --user-group \
    %{pkgname}

%post
%systemd_post taskd.service
%firewalld_reload

%preun
%systemd_preun taskd.service

%postun
%systemd_postun_with_restart taskd.service


%files
%doc AUTHORS COPYING NEWS ChangeLog
%{_bindir}/taskd
%{_bindir}/taskdctl
%{_mandir}/man1/taskd.1.*
%{_mandir}/man1/taskdctl.1.*
%{_mandir}/man5/taskdrc.5.*
%{taskd_datadir}/config

%dir %{taskd_confdir}
%config(noreplace) %{taskd_confdir}/config
%config(noreplace) %{taskd_confdir}/vars

%attr(-, %{pkgname}, %{pkgname}) %dir       %{taskd_logdir}
%attr(0644, root, root) %config(noreplace)  %{_sysconfdir}/logrotate.d/%{name}

%dir %{taskd_extdir}

%{_unitdir}/%{pkgname}.service
%{firewalld_servicedir}/%{pkgname}.xml

%dir %{pki_certdir}
%defattr(-, %{pkgname}, %{pkgname})
%{pki_scriptdir}/README
%{pki_scriptdir}/generate*
%{pki_scriptdir}/paths
%{pki_scriptdir}/vars

%defattr(0755, %{pkgname}, %{pkgname})
%dir %{taskd_datadir}
%dir %{taskd_datadir}/orgs

%changelog
* Sat Sep 08 2018 Jan Khardix Staněk <khardix@gmail.com> - 1.1.0-3
- Add which as dependency of generation scripts

* Sat Sep 08 2018 Jan Khardix Staněk <khardix@gmail.com> - 1.1.0-2
- BuildRequire firewalld-filesystem in order to expand %%post properly
- Make the service simple instead of forking -- avoid pid file troubles
- Apply system crypto policy only on supported OS

* Mon Sep 03 2018 Jan Staněk <jstanek@redhat.com> - 1.1.0-1
- Initial package
