<p style="text-align: center;"><strong>This source is not used in COPR builds!</strong></p>

The development of this package started in reaction to the absence
of both EPEL package or any up-to-date COPR for taskd.
Later, I found that the software is packaged in Fedora in up-to-date version,
and it is just not built for EPEL.
Building in COPR from <https://src.fedoraproject.org/rpms/taskd> works without any necessary changes,
and this package is therefore unneeded.

However, the inner structure of the (S)RPMs in this repository differs
from upstream (Fedora) (S)RPMs enough to leave it here, in case it will become needed later.

## Common features of both versions

Unmarked items are implemented only in upstream for now.

- [x] Compilation and installation of taskd binaries
- [x] Creation of dedicated user and run-time environment for the server
- [x] Installation of certificate generation scripts
- [ ] Provided SystemD service unit file
- [ ] Provided FirewallD service definition

## Changes implemented in this version

- Better compliance with [FHS][] and `file-hierarchy(7)` (namely generation scripts locations)
- Patched binaries to comply with [Fedora Crypto policies][]
- Patched certificate generation scripts that automatically use expected file system paths
    - Incomplete: Generated certificates have incorrect permissions (owned by root, unreadable by taskd)

[FHS]: https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard "Filesystem Hierarchy Standard"
[Fedora Crypto policies]: https://fedoraproject.org/wiki/Packaging:CryptoPolicies
